#!/bin/bash
################################################################################
#
# Filename: autostart_all_docker_mkdocs.sh
#   Author: Benjamin GUY-SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-09-17
#
################################################################################
#set -x

_PATH_TO_DOCKER_MKDOCS_DSP="${HOME}/b2c/my-own/distributor/dispenser"
_PATH_TO_DOCKER_MKDOCS_PRG="${HOME}/b2c/my-own/doc/programmation"
_PATH_TO_DOCKER_MKDOCS_PHY="${HOME}/b2c/my-own/doc/phytec"
_PATH_TO_DOCKER_MKDOCS_CTN="${HOME}/b2c/my-own/doc/container"
_PATH_TO_DOCKER_MKDOCS_OS="${HOME}/b2c/my-own/doc/os"
_PATH_TO_DOCKER_MKDOCS_ELEC="${HOME}/b2c/my-own/doc/electronique"

# On se déplace dans le dossier contenant les scripts pour que
# source ./libsh fonctionne.
cd ${HOME}/b2c/my-own/script/

# Mkdocs server
${HOME}/b2c/my-own/script/autostart_docker_mkdocs.sh $_PATH_TO_DOCKER_MKDOCS_DSP  'dispenser'      '8000'
${HOME}/b2c/my-own/script/autostart_docker_mkdocs.sh $_PATH_TO_DOCKER_MKDOCS_PRG  'programmation'  '8001'
${HOME}/b2c/my-own/script/autostart_docker_mkdocs.sh $_PATH_TO_DOCKER_MKDOCS_PHY  'phytec'         '8002'
${HOME}/b2c/my-own/script/autostart_docker_mkdocs.sh $_PATH_TO_DOCKER_MKDOCS_CTN  'container'      '8003'
${HOME}/b2c/my-own/script/autostart_docker_mkdocs.sh $_PATH_TO_DOCKER_MKDOCS_OS   'os'             '8004'
${HOME}/b2c/my-own/script/autostart_docker_mkdocs.sh $_PATH_TO_DOCKER_MKDOCS_ELEC 'electronique'   '8005'

# Après s'être déplacer (cd plus haut à la suite de la déclaration des CONTANTES)
# dans le dossier contenant les scripts, on revient dans le home pour l'utilisateur.
cd ${HOME}

exit 0
set +x
