#!/bin/sh
################################################################################
#
# filename: watcher_log.sh
#   author: Benjamin GUY-SOTTEAU
#     mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-07-05
#
################################################################################
# set -x

# Le nom du dossier sur le serveur ou stocker les fichiers.
dir_name_server='romond-lavage'

# Adresses du serveur brute et via DNS.
   _IP_SERVER='37.187.236.199'
_ALIAS_SERVER='web-coonguu1.tst.fra.optiweb-access.com'

# La cl'e RSA pour 
_PATH_RSA_KEY='/home/id_rsa_srv_ow'
_PATH_DIR_DISPENSER="/tmp"
_PATH_DIR_SERVER="/home/${dir_name_server}"

files="eGTouch_*.log"

for file in $files; do

  scp -i $_PATH_RSA_KEY ${_PATH_DIR_DISPENSER}/${file} \
  root@${_IP_SERVER}:${_PATH_DIR_SERVER}/${file}_$(date +%Y%m%d_%H%M%S).txt

done

# set +x
exit 0
