#!/bin/sh
################################################################################
#
# Filename: watcher_log_restart.sh
#   Author: Benjamin GUY-SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-07-12
#
################################################################################
#set -x

systemctl stop    watcher_log.timer
systemctl start   watcher_log.timer
systemctl status  watcher_log.timer

#set +x
exit 0
