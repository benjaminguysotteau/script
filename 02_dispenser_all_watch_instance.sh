#!/bin/bash
################################################################################
#
# Filename: dispenser_all_watch_instance.sh
#   Author: Benjamin GUY-SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-07-27
#
################################################################################
#set -x

################################################################################
#                                Environnement
################################################################################
# L'index du tableau correspond à l'id de la station,
# la string enregistré est affichée a coté de l'entête ticket.
declare -A a_dispenser_alt_names
a_dispenser_alt_names[02]='ELBEUF'
a_dispenser_alt_names[06]='DEVELOPMENT'
a_dispenser_alt_names[13]='MARINES'
a_dispenser_alt_names[18]='SAINT-WITZ'
a_dispenser_alt_names[22]='LES ECUELLES'
a_dispenser_alt_names[24]='BERGON'
a_dispenser_alt_names[25]='LE NAUTILUS'
a_dispenser_alt_names[26]='DUNOIS'
a_dispenser_alt_names[23]='GARAGE NICOLAS'
a_dispenser_alt_names[27]='ROMOND'
a_dispenser_alt_names[28]='PACY'
a_dispenser_alt_names[30]='BOUGUIN'
a_dispenser_alt_names[31]='AUDINCOURT'
a_dispenser_alt_names[32]='ERNEE'
a_dispenser_alt_names[33]='FLEXICOURT'
a_dispenser_alt_names[37]='FLEXICOURT B'
a_dispenser_alt_names[40]='GARAGE THOMAS'


################################################################################
#                                  Définiton 
################################################################################
DISPENSER_FILE_SSH_REVERSE_PORT='02_dispenser_all_ssh_reverse_port.csv'
_DISPENSER_FILE_UPTIME='00_dispenser_all_uptime.csv'

_DATE_N_HOUR_COLUMN_IN_FILE=1
_STATION_ID_COLUMN_IN_FILE=2
_SSH_PORT_NUMBER_COLUMN_IN_FILE=3
_SSH_PORT_STATE_COLUMN_IN_FILE=4

NUMBER_OF_LINES_AFTER_PRINT=11

get_dispenser_id()
{
  ls -1 | cut -d '-' -f 1 | egrep -v '(csv|sh)'
}

get_dispenser_name_friendly()
{
  dir_relative_path=$1
  dispenser_id=$2

  cat $dir_relative_path/${dispenser_id}_paramLog_*.txt | grep g_L1 | tail -n 1 | cut -d '=' -f 2

  unset dir_relative_path
  unset dispenser_id
}

get_dispenser_version()
{
  dir_relative_path=$1
  dispenser_id=$2

  cat $dir_relative_path/${dispenser_id}_paramLog_*.txt | grep version | tail -n 1 | cut -d '=' -f 2 | cut -d 'V' -f 2

  unset dir_relative_path
  unset dispenser_id
}

get_dispenser_event_last_month()
{
  dir_relative_path=$1
  dispenser_id=$2

  cat $dir_relative_path/${dispenser_id}_appliLog_*.txt | grep gx_TRANS | tail -n 1 | awk '{print $7}'

  unset dir_relative_path
  unset dispenser_id
}

get_dispenser_event_last_day()
{
  dir_relative_path=$1
  dispenser_id=$2

  cat $dir_relative_path/${dispenser_id}_appliLog_*.txt | grep gx_TRANS | tail -n 1 | awk '{print $6}'

  unset dir_relative_path
  unset dispenser_id
}

get_dispenser_event_last_hour()
{
  dir_relative_path=$1
  dispenser_id=$2

  cat $dir_relative_path/${dispenser_id}_appliLog_*.txt | grep gx_TRANS | tail -n 1 | awk '{print $9}'

  unset dir_relative_path
  unset dispenser_id
}

get_dispenser_event_last_action()
{
  dir_relative_path=$1
  dispenser_id=$2

  cat $dir_relative_path/${dispenser_id}_appliLog_*.txt | grep  "Ecriture distribution" | tail -n 1 | awk -F "VALUES" '{print $2}' | awk -F "," '{print $10}' | sed s/" '"/""/g | sed s/"'"/""/g

  unset dir_relative_path
  unset dispenser_id
}

get_dispenser_event_last_action_filtered()
{
  dir_relative_path=$1
  dispenser_id=$2

  cat $dir_relative_path/${dispenser_id}_appliLog_*.txt| grep  "Ecriture distribution" | tail -n 1 | awk -F "VALUES" '{print $2}' | awk -F "," '{print $10}' | sed s/" '"/""/g | sed s/"'"/""/g | awk -F "par " '{print $1}'

  unset dir_relative_path
  unset dispenser_id
}

get_dispenser_payout()
{
  payout="$*"
  a_modes="monnaie CB Billet"

  for mode in $a_modes
  do
    if [[ ! -z $(echo $payout | grep ${mode}) ]]; then
      printf "%-10s" "${mode}"
    fi
  done
}

# declare -a a_dispenser_ids=$(get_dispenser_id | tr '\n' ' ')
# pose probleme pour parcourir le tableau, bash comprend que tous les ids
# sont a l'index zero et ne split pas sur les espaces,
# donc simulation d'une structure en tableau avec la syntaxe de sh.
a_dispenser_ids=$(get_dispenser_id | tr '\n' ' ')

separator='|'
separator_with_pipe()
{
  printf "%2s" $separator && printf "%s" " "
}

get_timestamp_file()
{
  if [[ $# -ne 2 ]]; then
    echo "Error: get_timestamp_file() <dispenser-id>"
  fi

  dir_relative_path=$1
  id=$2

  # Pour la recherche:
  # - Récupérer les résultats du dernier jour uniquement
  # - Faire un ls puis un tail pour obtenir le denier fichier du jour si plusieurs existent.
  last_file_of_day=$(ls -1 $dir_relative_path/${id}_paramLog_*$(get_date_day_number_format)*.txt | tail -n 1)
  stat -c %Y $last_file_of_day

  unset dir_relative_path
  unset last_file_of_day
}

get_timestamp_current_time()
{
  date '+%s'
}

get_timestamp_diff_secondes()
{
  timestamp_current=$1
  timestamp_to_compare=$2

  echo $(($timestamp_current - $timestamp_to_compare))
}

get_number_of_transaction_error()
{
  dir_relative_path=$1
  id=$2

  grep gx_ERREUR $dir_relative_path/${id}_appliLog_*.txt | wc -l

  unset dir_relative_path
}

get_ssh_reverse_port_number()
{
  id=$1

  # On ajoute les ':' pour filtrer apres l'id pour que le grep
  # nous renvoie qu'une seule réponse possible.
  grep "id_${id};" ${DISPENSER_FILE_SSH_REVERSE_PORT} | cut -d ';' -f $_SSH_PORT_NUMBER_COLUMN_IN_FILE | tail -n 1
}

get_ssh_reverse_port_state()
{
  id=$1

  state=$(grep "id_${id};" ${DISPENSER_FILE_SSH_REVERSE_PORT} | cut -d ';' -f $_SSH_PORT_STATE_COLUMN_IN_FILE | tail -n 1)

  if   [[ "$state" == "o" ]]; then
    echo 'Open'
  else
    echo 'Close'
  fi
}

get_dispenser_uptime()
{
  id=$1

  cat $_DISPENSER_FILE_UPTIME | grep -i id_$id | awk '{print $4}' | cut -d ',' -f 1 | tail -n 1
}

get_date_year_name_format()
{
    # L'année au format YYYY
    date +%Y
}

get_date_month_name_format()
{
    # L'année au format yy
    date +%B
}

get_date_day_number_format()
{
    # Le jour au format dd
    date +%d
}

get_dir_relative_path()
{
  station_id=$1

  # Récuperer le nom de dossier parent composé de l'id de la station et un nom explicite
  # Récupérer l'année en cours
  # Récupérer le mois en cours
  dirs_name_w_id_n_name=$(ls -1 | egrep -v '(csv|sh)')
  dir_name_year=$(get_date_year_name_format)
  dir_name_month=$(get_date_month_name_format)

  # Concatener les 3 résultats pour obtenir le chemin relatif
  # Parcourir chaque dossier de station pour trouver une correspondance a l'id de la station
  for dir_name_w_id_n_name in ${dirs_name_w_id_n_name[@]}
  do
    # On récupére les deux premiers caractères qui correspond à l'id de la station
    if [ "${dir_name_w_id_n_name:0:2}" = "$station_id" ]; then
      echo $dir_name_w_id_n_name/$dir_name_year/$dir_name_month
    fi
  done
  
  unset station_id
  unset dirs_name_w_id_n_name
  unset dir_name_year
  unset dir_name_month
}


################################################################################
#                                  Exécution 
################################################################################
while :
do

  echo "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
  echo "| Id | En-tête ticket ligne 1         | Désignation    | Version | En vie      | Date dernier événement | Intéraction        | Réglement  | Cpt Err | Démarré | SSH Port | SSH State |"
  echo "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

  for id in $a_dispenser_ids
  do
    # Récupérer le chemin complet du dossier avec l'id de la station et son nom explicite
    dir_relative_path=$(get_dir_relative_path $id)

    payout=$(get_dispenser_event_last_action $dir_relative_path ${id})

    current_time=$(get_timestamp_current_time)
    dispenser_alive_time=$(get_timestamp_file $dir_relative_path ${id})

    result_diff_time_in_minutes="$(($(get_timestamp_diff_secondes ${current_time} ${dispenser_alive_time}) / 60))"
    result_diff_time_in_hours="$(($(get_timestamp_diff_secondes ${current_time} ${dispenser_alive_time}) / 3600))"
    result_diff_time_in_days="$(($(get_timestamp_diff_secondes ${current_time} ${dispenser_alive_time}) / 86400))"

    if [[ $result_diff_time_in_minutes -gt 30 ]]; then
      # On ecrit la ligne en orange puisque pas de log depuis plus de 30 minutes
      tput setaf 3
    fi

    if [[ $result_diff_time_in_minutes -gt 60 ]]; then
      # On ecrit la ligne en rouge puisque pas de log depuis plus de 60 minutes
      tput setaf 1
    fi

    # Id
    printf "%-2s" $separator
    printf "%2s"  ${id}
    separator_with_pipe

    # Ligne 1 en-tête ticket
    printf "%-30s" "$(get_dispenser_name_friendly $dir_relative_path ${id})"
    separator_with_pipe

    # Désignation
    printf "%-14s" "${a_dispenser_alt_names[$id]}"
    separator_with_pipe

    # Version
    printf "%7s" "$(get_dispenser_version $dir_relative_path ${id})"
    separator_with_pipe

    in_live=""
    # En vie
    if [[ $result_diff_time_in_days -gt 0 ]]; then
      in_live="${in_live} ${result_diff_time_in_days}j"
      
      result_diff_time_in_hours=$(($result_diff_time_in_hours % 24))
    fi

    if [[ $result_diff_time_in_hours -gt 0 ]]; then
      in_live="${in_live} ${result_diff_time_in_hours}h"

      result_diff_time_in_minutes=$(($result_diff_time_in_minutes % 60))
    fi

    if [[ $result_diff_time_in_minutes -ge 0 ]]; then
      in_live="${in_live} ${result_diff_time_in_minutes}m"
    fi

    printf "%11s" "${in_live}"
    unset in_live
    separator_with_pipe

    # Date événemement
    printf "%2s %-11s" "$(get_dispenser_event_last_day $dir_relative_path ${id})" "$(get_dispenser_event_last_month $dir_relative_path ${id})"
    separator_with_pipe

    printf "%5s" "$(get_dispenser_event_last_hour $dir_relative_path ${id})"
    separator_with_pipe

    # Intéraction
    printf "%-18s" "$(get_dispenser_event_last_action_filtered $dir_relative_path ${id})"
    separator_with_pipe

    # Réglement
    printf "%10s" "$(get_dispenser_payout ${payout})"
    separator_with_pipe

    # Compteur erreur transaction
    printf "%7s" "$(get_number_of_transaction_error $dir_relative_path ${id})"
    separator_with_pipe

    # Démarré
    printf "%7s" "$(get_dispenser_uptime $id)"
    separator_with_pipe

    # Port SSH numero
    printf "%8s" "$(get_ssh_reverse_port_number ${id})"
    separator_with_pipe

    # Port SSH statut
    printf "%-9s" "$(get_ssh_reverse_port_state ${id})"
    printf "%2s" $separator

    # On remet la couleur par défaut (blanc) du terminal
    tput sgr0

    printf "\n"
    printf "%s" "--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
    printf "\n"
    
    unset payout
  done

  # Lignes d'espacement apres l'affichage du tableau
  for j in $(seq 0 ${NUMBER_OF_LINES_AFTER_PRINT})
  do
    echo ''
  done

  sleep 60
done # while :

#set +x
exit 0
