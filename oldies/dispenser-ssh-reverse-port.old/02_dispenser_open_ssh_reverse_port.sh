#!/bin/bash
################################################################################
#
# Filename: 02_dispenser_open_ssh_reverse_port.sh
#   Author: Benjamin GUY-SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-08-27
#
################################################################################
#set -x

# SOURCE
# ------
# https://superuser.com/questions/352268/can-i-make-ssh-fail-when-a-port-forwarding-fails
# https://www.cyberciti.biz/tips/bash-shell-parameter-substitution-2.html
# https://www.cyberciti.biz/faq/bash-ksh-if-variable-is-not-defined-set-default-variable/
# https://unix.stackexchange.com/questions/60098/do-while-or-do-until-in-posix-shell-script
# https://stackoverflow.com/questions/1570262/get-exit-code-of-a-background-process
# https://www.maketecheasier.com/run-bash-commands-background-linux/
# https://stackoverflow.com/questions/9190151/how-to-run-a-shell-script-in-the-background-and-get-no-output
# https://stackoverflow.com/questions/3683910/executing-shell-command-in-background-from-script
# https://www.tecmint.com/run-linux-command-process-in-background-detach-process/

# Le binaire host n'est pas install'e sur les distributeurs
#0VH_SERVER_HOST_IP="$(host web-coonguu1.tst.fra.optiweb-access.com | awk '{print $4}')"

_LOCALHOST='127.0.0.1'

_OVH_SERVER_FROM_IP='37.187.236.199'
_OVH_SERVER_FROM_DNS='web-coonguu1.tst.fra.optiweb-access.com'

_PATH_TO_RSA_KEY='/home/id_rsa_srv_ow'

_SSH_USER='root'
_SSH_DEFAULT_PORT='22'
_SSH_PORT_MIN='22000'
_SSH_PORT_MAX='22099'

_DATE_N_HOUR_COLUMN_IN_FILE=1
_STATION_ID_COLUMN_IN_FILE=2
_SSH_PORT_COLUMN_IN_FILE=3

_DISPENSER_FILE_SSH_REVERSE_PORT='02_dispenser_all_ssh_reverse_port.csv'
_DISPENSER_CONVERT_FILE_PATH='01_dispenser_cnvrt_station_id_n_central_ip.csv' 

station_id='empty'
ssh_reverse_port="${1-$_SSH_PORT_MIN}" # argument 1 passe en parametre sinon port par defaut

write_ssh_reverse_port()
{
ssh -T $_OVH_SERVER_FROM_IP -i $_PATH_TO_RSA_KEY << EOSSH

echo '$(date +%y%m%d.%H%M%S);$station_id;$ssh_reverse_port;o' >> /home/log/$_DISPENSER_FILE_SSH_REVERSE_PORT

EOSSH
}

download_ssh_reverse_port_already_in_use()
{
  # Mettre le paramêtre -i juste après scp,
  # pour ne pas avoir le retour d'erreur:
  # Permission denied (publickey,gssapi-keyex,gssapi-with-mic).
  scp -i $_PATH_TO_RSA_KEY -p $_OVH_SERVER_FROM_IP:/home/log/$_DISPENSER_FILE_SSH_REVERSE_PORT /tmp
}

print_ssh_reverse_port_already_in_use()
{
  SSH_PORT_ALREADY_IN_USE=$(cat /tmp/$_DISPENSER_FILE_SSH_REVERSE_PORT | tr ' ' '\n')
  for line in ${SSH_PORT_ALREADY_IN_USE}
  do
    echo $line | cut -d ';' -f $_SSH_PORT_COLUMN_IN_FILE
  done
}

is_ssh_reverse_port_already_in_use()
{
  _SSH_PORT_ALREADY_IN_USE=$(cat /tmp/$_DISPENSER_FILE_SSH_REVERSE_PORT | tr ' ' '\n')
  ssh_port_new=$1

  for line in $_SSH_PORT_ALREADY_IN_USE
  do

    ssh_port_formatted=$(echo $line | cut -d ';' -f $_SSH_PORT_COLUMN_IN_FILE)

    if [[ "$ssh_port_new" == "$ssh_port_formatted" ]]; then
      return 1
    fi

    unset ssh_port_formatted

  done
  return 0
}

get_station_id()
{
  cat /tmp/$_DISPENSER_CONVERT_FILE_PATH | cut -d ';' -f 1
}

kill_ssh_reverse_port_already_open()
{
  pid_to_kill=$(ps | grep -i 'ssh -o StrictHostKeyChecking=no -o ExitOnForwardFailure=yes' | grep -v grep | awk '{print $1}')

  if [[ ! -z $pid_to_kill ]]; then
    kill -USR1 $pid_to_kill
  fi

  unset pid_to_kill
}

station_id=$(get_station_id)
download_ssh_reverse_port_already_in_use
kill_ssh_reverse_port_already_open

# On bouche à la recherche d'un port ssh disponible,
# si le port demandé est plus petit ou plus grand que la plage de ports autorisée, on quitte avec une erreur.
# On éxécute la commande de reverse ssh avec NOHUP pour détacher le processus fils du terminal qui éxécute ce script,
# on redirige les sorties standards dans /DEV/NULL pour ne pas avoir la création du fichier nohup.out.
# On attend que la commande reverse ssh soit terminée en passant son PID à wait.
# On éxécute les instructions de la boucle while au moins une fois.
while 

  # Si le port reverse ssh est deja en cours d'utilisation,
  # parmi la plage de port ssh autorisée,
  # on incremente $ssh_reverse_port jusqu'à temps de trouver un port ssh libre.
  while [[ $(is_ssh_reverse_port_already_in_use $ssh_reverse_port; echo $?) -eq 1 ]] \
     && [[ $ssh_reverse_port -ge $_SSH_PORT_MIN ]] \
     && [[ $ssh_reverse_port -le $_SSH_PORT_MAX ]]
  do
    echo "Warning: $0, $ssh_reverse_port already in use!"
    let "ssh_reverse_port++"
  done

  if [[ $ssh_reverse_port -lt $_SSH_PORT_MIN ]] \
  || [[ $ssh_reverse_port -gt $_SSH_PORT_MAX ]]; then
    echo "Error: $0, $ssh_reverse_port is out of define range ports!"
    exit 1
  fi

  SSH_REVERSE_QUERY="ssh -o StrictHostKeyChecking=no -o ExitOnForwardFailure=yes \
                     -fN -R $ssh_reverse_port:$_LOCALHOST:$_SSH_DEFAULT_PORT \
                     $_SSH_USER@$_OVH_SERVER_FROM_IP -i $_PATH_TO_RSA_KEY"

  ${SSH_REVERSE_QUERY}

  if [[ "$?" -eq 0 ]]; then
    echo "Ouverture du port: $ssh_reverse_port."
    write_ssh_reverse_port
    break
  else
    let "ssh_reverse_port++"
    continue
  fi

  sleep 1

  [ "$?" -ne 0 ]
do :; done

#set +x
exit 0
