#!/bin/bash
################################################################################
#
# Filename: server_move_and_merge_log_file.sh
#   Author: Benjamin GUY-SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-10-19
#
################################################################################

# Les logs sont contenus dans un dossier parent qui à pour nom
# l'id et l'entête ticket de la station,
# puis un dossier correspondant à l'année,
# puis un dossier correspondant au mois,
# puis un fichier de log par jour contenu dans mois.

################################################################################
#                               Définition
################################################################################
to_lower_case()
{
    str_to_lower=$*

    echo $str_to_lower | tr '[:upper:]' '[:lower:]'
}

to_dash_from_space()
{
    str_to_dash_from_space=$*

    echo $str_to_dash_from_space | tr ' ' '-'
}

get_logfile_name_for_the_day()
{
    station_id=$1
    logfile_name=$2
    date_year=$3
    date_month=$4
    date_day=$5
    date=$date_year$date_month$date_day

    # Ajouter le tiret après ${date} permet si le fichier existe déjà
    # de l'exclure du résultat de la recherche.
    ls -1 | grep ${station_id}_ | grep $logfile_name | grep ${date}-
}

get_header_ticket_from_station_id()
{
    station_id=$1

    # On utilise la commande uniq dans le cas ou il y aurait au moins un redemarrage dans la journée
    cat ${station_id}_paramLog_*.txt | grep g_L1 | uniq | cut -d '=' -f 2
}

get_date_year_four()
{
    # L'année au format YYYY
    date +%Y
}

get_date_year_two()
{
    # L'année au format YY
    date +%y
}

get_date_month_name()
{
    # Le mois au format <month-name>
    date +%B
}

get_date_month_number()
{
    # Le mois au format XX
    date +%m
}

get_date_day_number()
{
    # Le jour au format XX
    date +%d
}

create_dir_if_does_not_exists()
{
    directory_name=$1

    if [ ! -d $directory_name ]; then
	mkdir -p $directory_name
    else	
	echo "$directory_name already exists."
    fi
}

print_debug_script_variable()
{
    if [ "$_DEBUG" = "true" ]; then
	echo -e "Entête ticket brûte: \t\t $res_header_ticket_from_station_id"
	echo -e "Entête convertion tirets: \t $res_to_dash_from_space"
	echo -e "Entête convertion minuscules: \t $res_to_lower_case"
	echo -e "Entête ajout identifiant: \t $str_station_id_n_header_ticket"
	echo -e "Année au format 4 caractères: \t $_DATE_YEAR_FOUR"
	echo -e "Année au format 2 caractères: \t $_DATE_YEAR_TWO"
	echo -e "Mois au format nom: \t\t $_DATE_MONTH_NAME"
	echo -e "Mois au format nombre: \t\t $_DATE_MONTH_NUMBER"
	echo -e "Jour au format nombre: \t\t $_DATE_DAY"
	echo -e "Nom fichier de log: \t\t $logfile_name"
    fi
}

convert_timestamp_file_to_date()
{
    file_for_timestamp=$1
    timestamp_to_date=$2
    
    timestamp_file=$(stat -c %Y $file_for_timestamp)
    [ "$_DEBUG" = "true" ] && echo $timestamp_file

    if   [ "$timestamp_to_date" = "year" ]; then
	date -d @$timestamp_file +"%Y"
	
    elif [ "$timestamp_to_date" = "month" ]; then
	date -d @$timestamp_file +"%B"

    else
	echo "Error: $0, convert_timestamp_file_to_date, timestamp_to_date: $timestamp_to_date incorrect!"
	exit 1
    fi
}

convert_logfile_in_one()
{
    logfile_output=$1

    # Si le fichier existe déjà, on ne traite pas l'instruction
    # pour ne pas ajouter le contenu en double.
    # Sinon un génére le fichier.
    if [ ! -f $logfile_output ]; then
	for logfile_in_one in $(get_logfile_name_for_the_day $station_id $logfile_name $_DATE_YEAR_TWO $_DATE_MONTH_NUMBER $_DATE_DAY)
	do
	    [ "$_DEBUG" = "true" ] && echo $logfile_in_one
	    cat $logfile_in_one >> $logfile_output
	done
    fi
}

move_logfile_to_correct_folder()
{
    # On déplace les fichiers de logs correspondant au mois.
    # ---
    # On récupére le timestamp du fichier et:
    # - on le converti en année,
    # - on le converti en mois.
    # On déplace les fichiers trouvés dans le dossier année et mois.
    for logfile_to_folder in $(get_logfile_name_for_the_day $station_id $logfile_name $_DATE_YEAR_TWO $_DATE_MONTH_NUMBER $_DATE_DAY)
    do
	[ "$_DEBUG" = "true" ] && echo $logfile_to_folder
	
	#convert_timestamp_file_to_date $logfile_to_folder year
	#convert_timestamp_file_to_date $logfile_to_folder month
	
	mv $logfile_to_folder $str_station_id_n_header_ticket/$_DATE_YEAR_FOUR/$_DATE_MONTH_NAME
    done
}

################################################################################
#                               Execution 
################################################################################
station_id='06'
logfile_name='appliLog'

# CONSTANTES
_DEBUG="false"
_DATE_YEAR_FOUR=$(get_date_year_four)
_DATE_YEAR_TWO=$(get_date_year_two)
_DATE_MONTH_NAME=$(get_date_month_name)
_DATE_MONTH_NUMBER=$(get_date_month_number)
_DATE_DAY=$(get_date_day_number)

# On récupére pour une station son paramLog qui contient la ligne d'entête ticket,
# utilisée pour la création de son dossier ou seront contenu les logs.
res_header_ticket_from_station_id=$(get_header_ticket_from_station_id $station_id)

# Une fois l'entête ticket,
# on remplace les espaces par des tirets,
# on converti les caratères majuscules en minuscules.
# on ajoute l'id de la station a la nouvelle chaine de caractères
res_to_dash_from_space=$(to_dash_from_space $res_header_ticket_from_station_id)
res_to_lower_case=$(to_lower_case $res_to_dash_from_space)
str_station_id_n_header_ticket=${station_id}-${res_to_lower_case}

print_debug_script_variable

# On utilise la chaine pour créer le dossier s'il n'existe pas,
# on créer le sous dossier de l'année,
# on créer le sous dossier du mois,
create_dir_if_does_not_exists $str_station_id_n_header_ticket
create_dir_if_does_not_exists $str_station_id_n_header_ticket/$_DATE_YEAR_FOUR
create_dir_if_does_not_exists $str_station_id_n_header_ticket/$_DATE_YEAR_FOUR/$_DATE_MONTH_NAME

# On exécute une recherche avec la date au format YYMMDD
_DATE=${_DATE_YEAR_TWO}${_DATE_MONTH_NUMBER}${_DATE_DAY}

logfile_output=${station_id}_${logfile_name}_${_DATE}.txt
[ "$_DEBUG" = "true" ] && echo $logfile_output

convert_logfile_in_one $logfile_output
move_logfile_to_correct_folder

set +x
exit 0
