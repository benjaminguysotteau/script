#!/bin/bash
################################################################################
#
# Filename: dispenser_all_save_logs.sh
#   Author: Benjamin GUY-SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-09-04
#
################################################################################
#set -x

get_dispenser_id()
{
  ls -1 *.txt | cut -d '_' -f 1 | uniq
}

get_dispenser_name_friendly()
{
  dispenser_id=$1

  cat ${dispenser_id}_paramLog.txt | grep g_L1 | cut -d '=' -f 2
}

to_lower()
{
  str_to_lower=$1

  echo $str_to_lower | tr '[:upper:]' '[:lower:]'
}

# declare -a a_dispenser_ids=$(get_dispenser_id | tr '\n' ' ')
# pose probleme pour parcourir le tableau, bash comprend que tous les ids
# sont a l'index zero et ne split pas sur les espaces,
# donc simulation d'une structure en tableau avec la syntaxe de sh.
a_dispenser_ids=$(get_dispenser_id | tr '\n' ' ')

declare -a a_log_files=('appliLog_*'        \
                        'paramLog_*'        \
                        'another_release' )
a_log_file_length=${#a_log_files[@]}
a_log_file_length_minus_1=$((a_log_file_length - 1))

# Pour chacun des distributeurs,
for id in $a_dispenser_ids
do
  str_to_lower=$(to_lower "$(get_dispenser_name_friendly $id)")
  str_lower_w_dash=${str_to_lower//[ ]/-}
  str_lower_w_dash_n_id="${id}-${str_lower_w_dash}"

  # Vérifier que le dossier existe,
  # si le dossier n'existe pas, on le créer depuis l'id du distributeur et son nom:
  if [[ ! -d ${str_lower_w_dash_n_id} ]]; then
    echo "Creation du dossier: ${str_lower_w_dash_n_id}"
    mkdir ${str_lower_w_dash_n_id}
  fi

  # Le dossier existe,
  # on sauvegarde pour chaque distributeur les fichiers de logs,
  # en y ajoutant la date et l'heure de la sauvegarde du fichier en préfix:
  for i in $(seq 0 ${a_log_file_length_minus_1})
  do
    log_file=${id}_${a_log_files[${i}]}.txt
    log_destination_folder=${str_lower_w_dash_n_id}

    if [[ -f $log_file ]]; then
      cp -a $log_file $log_destination_folder/$(date +%y%m%d_%H%M%S)_$log_file
    else
      echo "Warning: $0, $log_file doesn't exists!"
    fi
    
    unset log_file
    unset log_destination_folder
  done

  unset str_to_lower
  unset str_lower_w_dash
  unset str_lower_w_dash_n_id
done

set +x
exit 0
