#!/bin/bash
################################################################################
#
# Filename: dispenser_send_ascii_convert_hexa_to_tty.sh
#   Author: Benjamin GUY-SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-09-14
#
################################################################################
#set -x

#if [[ $# -ne 3 ]]; then
#  echo "Usage: $0, <verb> <text-in-ascii> <tty-number>"
#fi

convert_ascii_to_hexa()
{
  set -x
  str_ascii_raw=$1
  set +x

  str_hexa_with_spaces=$(echo $str_ascii_raw | hexdump -C | cut -d '|' -f 1 | awk '{print substr($0, index($0, $2))}' | sed -n 1p)
  convert_ascii_to_hexa=$(echo $str_hexa_with_spaces | sed 's: :\\x:g')
 
  echo $convert_ascii_to_hexa
}

print_hexadump_interpretation()
{
  set -x
  str_ascii_raw=$1
  set +x

  str_hexa_interpretation=$(echo $str_ascii_raw | hexdump -C | cut -d '|' -f 2 | sed -n 1p)

  echo $str_hexa_interpretation
}

send_hexa_to_tty()
{
  set -x
  hexa_to_send="$1"
  tty_number=$2
  set +x
  
  echo -en "$hexa_to_send" > /dev/tty$tty_number
}

_VERB=$1
if [[ "$_VERB" == "send" ]]; then
  _STR_ASCII=$2
  _TTY_NUMBER=$3

  return_convert_ascii_to_hexa=$(convert_ascii_to_hexa "$_STR_ASCII")
  send_hexa_to_tty "$return_convert_ascii_to_hexa" $_TTY_NUMBER
fi

if [[ "$_VERB" == "print" ]]; then
set -x
  _TTY_NUMBER=$2

  echo -en '\xOD' > /dev/tty$_TTY_NUMBER
set +x
fi

if [[ "$_VERB" == "cut" ]]; then
  echo -en '\x1B' '\x66' '\x'
fi

set +x
exit 0
