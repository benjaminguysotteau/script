#!/bin/bash
################################################################################
#
# Filename: autostart_google.sh
#   Author: Benjamin GUY SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-07-03
#
################################################################################
#set -x

# Pour que ./libsh/ fonctionne il faut faire
# un cd dans le fichier appelant autostart.sh
source ./libsh/print.sh

# SOURCE
# ------
# https://peter.sh/experiments/chromium-command-line-switches/#load-extension

google_chrome=$(which google-chrome)

if [[ ! -f $google_chrome ]]; then
  echo "$0, error: Google Chrome webbrowser not found!"
  exit 1
fi

PATH_TO_MY_STDERR="${HOME}/b2c/.mystderr"

if [[ ! -z $(ps -aux | grep -v grep | grep -i chrome) ]]; then
  echo -n "[$(date +%y%m%d).$(date +%H%M%S)] -- $0 -- " >> $PATH_TO_MY_STDERR
  echo    "Chrome already running!"                     >> $PATH_TO_MY_STDERR
  exit 2
fi

print_formatted info "Démarrage de google-chrome avec les pages utiles."

declare -a url=('https://ex-mail.biz' \
                'https://bitbucket.org/dashboard/overview' \
                'https://centerpay.atlassian.net/secure/Dashboard.jspa' \
                'https://hub.docker.com/' \
                'http://srv-prod:8089/open/login' \
                'https://www2.lyra-network.com/rft/login.jsp' \
                'https://beta-heurtaux.siqual.net/admin/utilisateurs/' \
                'https://beta-heurtaux.siqual.net/login' \
                'https://www.optiweb-access.fr/admin/login' \
                'https://www.optiweb-access.fr/login')
url_length=${#url[@]}
url_length_minus_1=$((url_length - 1))

declare -a a_localhost_ports=('8000' \
                              '8001' \
                              '8002' \
                              '8003' \
                              '8004' \
                              '8005' )
a_localhost_ports_length=${#a_localhost_ports[@]}
a_localhost_ports_length_minus_1=$((a_localhost_ports_length - 1))

for j in $(seq 0 $a_localhost_ports_length_minus_1)
do
  $google_chrome --password-store=basic http://localhost:${a_localhost_ports[j]} 1> /dev/null 2>&1 &
  sleep 0.5
done

# Sleep sur 20 secondes pour avoir les buffers chrome dans le bon ordre.
sleep 20

for i in $(seq 0 $url_length_minus_1)
do
  # TODO:
  # - pinned tab

  # --password-storage='basic|gnome|kdewallet'
  # --password-storage=gnome -> Ask for root password to unlock keyring.
  # --password-storage=basic -> Works.
  $google_chrome --incognito --password-store=basic ${url[i]}  1> /dev/null 2>&1 &
  sleep 0.5
done

set +x
exit 0
