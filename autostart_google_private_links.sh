#!/bin/bash
################################################################################
#
# Filename: autostart_google_private_links.sh
#   Author: Benjamin GUY SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-09-17
#
################################################################################
#set -x

# Pour que ./libsh/ fonctionne il faut faire
# un cd dans le fichier appelant autostart.sh
source ./libsh/print.sh

# SOURCE
# ------
# https://peter.sh/experiments/chromium-command-line-switches/#load-extension

google_chrome=$(which google-chrome)

if [[ ! -f $google_chrome ]]; then
  echo "$0, error: Google Chrome webbrowser not found!"
  exit 1
fi

PATH_TO_MY_STDERR="${HOME}/b2c/.mystderr"

#if [[ ! -z $(ps -aux | grep -v grep | grep -i chrome) ]]; then
#  echo -n "[$(date +%y%m%d).$(date +%H%M%S)] -- $0 -- " >> $PATH_TO_MY_STDERR
#  echo    "Chrome already running!"                     >> $PATH_TO_MY_STDERR
#  exit 2
#fi

#set -x
print_formatted info "Démarrage de google-chrome avec les pages utiles."

declare -a url=('https://ex-mail.biz' \
                'https://bitbucket.org/dashboard/overview' \
                'https://centerpay.atlassian.net/projects/DIS?orderField=RANK&selectedItem=com.atlassian.jira.jira-projects-plugin%3Arelease-page&status=all' \
                'https://centerpay.atlassian.net/wiki/discover/all-updates' \
                'https://hub.docker.com/' \
                'http://srv-prod:8089/open/login' \
                'https://www2.lyra-network.com/rft/login.jsp' \
                'https://beta-heurtaux.siqual.net/admin/utilisateurs/' \
                'https://beta-heurtaux.siqual.net/login' \
                'https://www.optiweb-access.fr/admin/login' \
                'https://www.optiweb-access.fr/login')
url_length=${#url[@]}
url_length_minus_1=$((url_length - 1))

for i in $(seq 0 $url_length_minus_1)
do
  $google_chrome --incognito --password-store=basic ${url[i]}  1> /dev/null 2>&1 &
  sleep 0.5
done

set +x
exit 0
