#!/bin/bash
################################################################################
#
# Filename: autostart_mkdocs.sh
#   Author: Benjamin GUY-SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-07-06
#
################################################################################
#set -x

# Pour que ./libsh/ fonctionne il faut faire
# un cd dans le fichier appelant autostart.sh
source ./libsh/print.sh

if [[ $# -ne 1 ]]; then
  echo "Usage: $0 <path-to-mkdocs-documentation>"
  exit 1
fi

PATH_TO_MY_STDERR="${HOME}/b2c/.mystderr"

# if [[ ! -z $(ps -aux | grep -v grep | grep -i mkdocs) ]]; then
# est vrai puisque au moment de l'éxécution est trouvé en résultat autostart_mkdocs.sh,
# donc rajouter serve pour filtrer sur le bon élément.
if [[ ! -z $(ps -aux | grep -v grep | grep -i 'mkdocs serve') ]]; then
  echo -n "[$(date +%y%m%d).$(date +%H%M%S)] -- $0 -- " >> $PATH_TO_MY_STDERR
  echo    "Mkdocs already running!"                     >> $PATH_TO_MY_STDERR
  exit 2
fi

# PATH_TO_DOC="${HOME}/b2c/my-own/distributor/doc"
PATH_TO_DOC="$1"

# Selon les versions de ubuntu le binaire mkdocs n'est pas dans le $PATH,
# si tel est le cas, on specifie le path de mkdocs,
if [[ -z $(which mkdocs) ]]; then
  binary="${HOME}/.local/bin/mkdocs"
else
  binary='mkdocs'
fi

print_formatted info "Démarrage du serveur Mkdocs."
cd $PATH_TO_DOC
# On redirige la sortie standard vers /dev/null ainsi que la sortie d'erreur.
$binary serve 1> /dev/null 2>&1 &

#set +x
#exit 0
