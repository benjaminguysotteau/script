#!/bin/bash
################################################################################
#
# Filename: autostart_docker_mkdocs.sh
#   Author: Benjamin GUY-SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-07-23
#
################################################################################
#set -x

# Pour que ./libsh/ fonctionne il faut faire
# un cd dans le fichier appelant autostart.sh
source ./libsh/print.sh

if [[ $# -ne 3 ]]; then
  print_formatted error "Usage: $0, <path-to-doc> <doc-name> <doc-port>"
  exit 2
fi

PATH_TO_DOCUMENTATION="$1"

DOCUMENTATION_NAME="$2"
DOCKER_PORT_NUMBER="$3"

PATH_TO_MY_STDERR="${HOME}/b2c/.mystderr"

if [[ ! -z $(docker ps | grep ${DOCUMENTATION_NAME}) ]]; then
  echo -n "[$(date +%y%m%d).$(date +%H%M%S)] -- $0 -- " >> $PATH_TO_MY_STDERR
  echo    "Mkdocs Docker already running!"              >> $PATH_TO_MY_STDERR
  exit 1
fi

print_formatted info "Démarrage du Docker Mkdocs ${DOCUMENTATION_NAME}."

# On attribut un nom d'identification et un nom de host au container
# on expose le port de notre choix et on se connecte dessus,
# on surchage l'entrypoint avec /bin/sh du conteneur,
# et on lui fait executer la commande avec -c:
# mkdocs serve --dev-addr=0.0.0.0:${DOCKER_PORT_NUMBER}
docker run -d  --rm --name ${DOCUMENTATION_NAME} -h ${DOCUMENTATION_NAME} \
--expose=${DOCKER_PORT_NUMBER} -p ${DOCKER_PORT_NUMBER}:${DOCKER_PORT_NUMBER} \
--entrypoint='/bin/sh' -v ${PATH_TO_DOCUMENTATION}:/docs \
squidfunk/mkdocs-material -c "mkdocs serve --dev-addr=0.0.0.0:${DOCKER_PORT_NUMBER}" 1> /dev/null

#set +x
#exit 0
