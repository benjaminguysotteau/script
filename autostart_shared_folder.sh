#!/bin/bash
################################################################################
#
# Filename: autostart_shared_folder.sh
#   Author: Benjamin GUY-SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-07-06
#
################################################################################
#set -x

# Pour que ./libsh/ fonctionne il faut faire
# un cd dans le fichier appelant autostart.sh
source ./libsh/print.sh

if [[ $# -ne 1 ]];then
  echo "Usage: $0, <path-to-virtualbox-shared-folder>"
  exit 1
fi

PATH_TO_VIRTUALBOX_SHARED_FOLDER=$1

# On compte le nombre de lignes du dossier partagé,
# si le nombre de ligne est égale à 3,
# alors on a que les chemins "." et ".." et le retour chariot,
# donc le dossier n'est pas monté.
if [[ $(ls -la $PATH_TO_VIRTUALBOX_SHARED_FOLDER | wc -l) -eq 3 ]]; then
  print_formatted info 'Demande sudo pour montage du dossier partagé virtualbox:'
  sudo mount -t vboxsf -o uid=${UID},gid=$(id -g) tmp $PATH_TO_VIRTUALBOX_SHARED_FOLDER
fi

#set +x
#exit 0
