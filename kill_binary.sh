#!/bin/bash
################################################################################
#
# Filename: kill_binary.sh
#   Author: Benjamin GUY SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-06-28
#
################################################################################
#set -x

if [[ $# -gt 1 ]]; then
  echo "Usage: $0 [binary-name]"
  exit 1
fi

if [[ $# -eq 0 ]]; then
  binary='distributeur'
else
  binary=$1
fi

kill -9 $(ps | grep -v grep | grep -i $binary | awk '{print $1}')

#set +x
exit 0
