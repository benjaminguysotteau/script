#!/bin/bash
################################################################################
#
# Filename: dispenser_write_uptime_to_ovh_server.sh
#   Author: Benjamin GUY-SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-09-06
#
################################################################################
#set -x

DISPENSER_FILE_UPTIME='00_dispenser_all_uptime.csv'
OVH_SERVER_FROM_IP='37.187.236.199'
PATH_TO_RSA_KEY='/home/id_rsa_srv_ow'

_DISPENSER_CONVERT_FILE_PATH='01_dispenser_cnvrt_station_id_n_central_ip.csv' 

get_station_id()
{
  cat /tmp/$_DISPENSER_CONVERT_FILE_PATH | cut -d ';' -f 1
}

write_uptime_to_ovh_server()
{
  date=$1
  uptime=$2
  id=$3

ssh -T ${OVH_SERVER_FROM_IP} -i ${PATH_TO_RSA_KEY} << EOSSH
echo "$date;$id;$uptime" >> /home/log/${DISPENSER_FILE_UPTIME}
EOSSH
}

write_uptime_to_ovh_server $(date +%y%m%d.%H%M%S) "$(uptime)" $(get_station_id)

set +x
exit 0
