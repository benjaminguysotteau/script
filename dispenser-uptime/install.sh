#!/bin/bash
################################################################################
#
# Filename: install.sh
#   Author: Benjamin GUY-SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-09-14
#
################################################################################
#set -x

_WRITE_UPTIME_TO_OVH_SERVER_TIMER='dispenser_write_uptime_to_ovh_server.timer'
_WRITE_UPTIME_TO_OVH_SERVER_SCRIPT='dispenser_write_uptime_to_ovh_server.sh'
_WRITE_UPTIME_TO_OVH_SERVER_SERVICE='dispenser_write_uptime_to_ovh_server.service'

_PATH_PLACEHOLDER_SCRIPT='/home'
_PATH_PLACEHOLDER_SYSTEMD='/etc/systemd/system'

# Supprimer les scripts déjà présents pour repartir d'une base propre.
rm $_PATH_PLACEHOLDER_SCRIPT/$_WRITE_UPTIME_TO_OVH_SERVER_SCRIPT

# Supprimer les services déjà présents pour repartir d'une base propre.
rm $_PATH_PLACEHOLDER_SYSTEMD/$_WRITE_UPTIME_TO_OVH_SERVER_SERVICE

# Supprimer les timers   déjà présents pour repartir d'une base propre.
rm $_PATH_PLACEHOLDER_SYSTEMD/$_WRITE_UPTIME_TO_OVH_SERVER_TIMER

# Copier les scripts dans le fichier /home.
cp -a $_WRITE_UPTIME_TO_OVH_SERVER_SCRIPT $_PATH_PLACEHOLDER_SCRIPT

# Copier les services dans  /etc/systemd/system.
cp -a $_WRITE_UPTIME_TO_OVH_SERVER_SERVICE $_PATH_PLACEHOLDER_SYSTEMD

# Copier les timers   dans /etc/systemd/system.
cp -a $_WRITE_UPTIME_TO_OVH_SERVER_TIMER $_PATH_PLACEHOLDER_SYSTEMD

# Créer les liens symboliques pour l'éxécution des timers au démarrage du distributeur.
systemctl enable $_WRITE_UPTIME_TO_OVH_SERVER_TIMER

set +x
exit 0
