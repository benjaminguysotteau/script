#!/bin/bash
################################################################################
#
# Filename: install.sh
#   Author: Benjamin GUY-SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-09-13
#
################################################################################
#set -x

################################################################################
#                                  Environment
################################################################################
_SCRIPT_SSH_SNIFF_STATION_ID_N_IP='00_dispenser_sniff_station_id_n_central_ip.sh'
_SCRIPT_SSH_CONVERT_STATION_ID_N_IP='01_dispenser_convert_station_id_n_central_ip.sh'

_SSH_SNIFF_STATION_ID_N_IP_TIMER='dispenser_sniff_station_id_n_central_ip.timer'
_SSH_SNIFF_STATION_ID_N_IP_SERVICE='dispenser_sniff_station_id_n_central_ip.service'

_SSH_CONVERT_STATION_ID_N_IP_TIMER='dispenser_convert_station_id_n_central_ip.timer'
_SSH_CONVERT_STATION_ID_N_IP_SERVICE='dispenser_convert_station_id_n_central_ip.service'

_SSH_REVERSE_PORT_CONFIG='dispenser_ssh_reverse_port.service.d'
_SSH_REVERSE_PORT_SERVICE='dispenser_ssh_reverse_port.service'

_PATH_PLACEHOLDER_SCRIPT='/home'
_PATH_PLACEHOLDER_SYSTEMD='/etc/systemd/system'


################################################################################
#                                  Execution
################################################################################


####################### On supprime les anciens fichiers #######################

# Supprimer les scripts déjà présents pour repartir d'une base propre.
rm $_PATH_PLACEHOLDER_SCRIPT/$_SCRIPT_SSH_SNIFF_STATION_ID_N_IP
rm $_PATH_PLACEHOLDER_SCRIPT/$_SCRIPT_SSH_CONVERT_STATION_ID_N_IP

# Supprimer les services déjà présents pour repartir d'une base propre.
rm $_PATH_PLACEHOLDER_SYSTEMD/$_SSH_SNIFF_STATION_ID_N_IP_SERVICE
rm $_PATH_PLACEHOLDER_SYSTEMD/$_SSH_CONVERT_STATION_ID_N_IP_SERVICE

# Supprimer les timers   déjà présents pour repartir d'une base propre.
rm $_PATH_PLACEHOLDER_SYSTEMD/$_SSH_CONVERT_STATION_ID_N_IP_TIMER
rm $_PATH_PLACEHOLDER_SYSTEMD/$_SSH_SNIFF_STATION_ID_N_IP_TIMER


####################### On copie les nouveaux fichiers #########################

# Copier les scripts dans le fichier /home.
cp -a $_SCRIPT_SSH_SNIFF_STATION_ID_N_IP    $_PATH_PLACEHOLDER_SCRIPT
cp -a $_SCRIPT_SSH_CONVERT_STATION_ID_N_IP  $_PATH_PLACEHOLDER_SCRIPT

# Copier les configuration dans /etc/systemd/system.
cp -a $_SSH_REVERSE_PORT_CONFIG             $_PATH_PLACEHOLDER_SYSTEMD

# Copier les services dans  /etc/systemd/system.
cp -a $_SSH_SNIFF_STATION_ID_N_IP_SERVICE   $_PATH_PLACEHOLDER_SYSTEMD
cp -a $_SSH_CONVERT_STATION_ID_N_IP_SERVICE $_PATH_PLACEHOLDER_SYSTEMD
cp -a $_SSH_REVERSE_PORT_SERVICE            $_PATH_PLACEHOLDER_SYSTEMD

# Copier les timers   dans /etc/systemd/system.
cp -a $_SSH_SNIFF_STATION_ID_N_IP_TIMER     $_PATH_PLACEHOLDER_SYSTEMD
cp -a $_SSH_CONVERT_STATION_ID_N_IP_TIMER   $_PATH_PLACEHOLDER_SYSTEMD


####################### On active les nouveaux services et timers ##############

# Créer les liens symboliques pour l'éxécution des timers au démarrage du distributeur.
systemctl enable $_SSH_SNIFF_STATION_ID_N_IP_TIMER
systemctl enable $_SSH_CONVERT_STATION_ID_N_IP_TIMER
systemctl enable $_SSH_REVERSE_PORT_SERVICE

set +x
exit 0
