#!/bin/bash
################################################################################
#
# Filename: 01_dispenser_convert_station_id_n_central_ip.sh
#   Author: Benjamin GUY-SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-09-
#
################################################################################
#set -x

_CANDUMP_RESULT_FILE_PATH='00_dispenser_sniff_station_id_n_central_ip.csv' 
_CANDUMP_CONVRT_FILE_PATH='01_dispenser_cnvrt_station_id_n_central_ip.csv' 

_CANDUMP_RESULT_CONTENT=$(cat /tmp/$_CANDUMP_RESULT_FILE_PATH)

_IP_INDEX_BGN=2
_IP_INDEX_END=5

# L -> LOW  = Le bit de poids faible
# H -> HIGH = Le bit de poids fort
_ID_STATION_L=6
_ID_STATION_H=7

format_candump_result_content()
{
  candump_file_path=$1

  candump_result_content=$(cat $candump_file_path | cut -d ']' -f 2 | cut -d "'" -f 1)
  candump_result_content=$(echo $candump_result_content | tr '  ' ' ')
}

convert_candump_result_content()
{
  hex_value=$1

  echo "ibase=16; $hex_value" | bc
}

format_candump_result_content /tmp/$_CANDUMP_RESULT_FILE_PATH

index=0
ip_concatenation=''
id_concatenation=''
for hex in $candump_result_content
do

  # ToDo
  # Faire la convertion,
  # avec le bit de poids fort,
  # tl   le bit de poids faible.
  #if [[ $index -eq $_ID_STATION_L ]] \
  #&& [[ $index -le $_ID_STATION_H ]]; then
  #  id_concatenation=$($(convert_candump_result_content (($hex + 1)))$(convert_candump_result_content $hex))
  #fi
  
  # Apres convertion bit poids faible, fort,
  # il faudra remplacer ce if
  if [[ $index -eq $_ID_STATION_L ]]; then
    id_concatenation=$(convert_candump_result_content $hex) 
  fi

  if [[ $index -ge $_IP_INDEX_BGN ]] \
  && [[ $index -le $_IP_INDEX_END ]]; then
    ip_concatenation+=$(convert_candump_result_content $hex)

    if [[ $index -ne $_IP_INDEX_END ]]; then
      ip_concatenation+='.'
    fi

  fi
  let "index++"

done

echo "id_$id_concatenation;$ip_concatenation"  > /tmp/$_CANDUMP_CONVRT_FILE_PATH

set +x
exit 0
