#!/bin/bash
################################################################################
#
# Filename; 00_dispenser_sniff_station_id_n_central_ip.sh
#   Author: Benjamin GUY-SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-09-
#
################################################################################
#set -x

_FILE_PATH_CANDUMP_RESULT='00_dispenser_sniff_station_id_n_central_ip.csv'

_CANDUMP_QUERY="candump -e can0,18A:7FF -acx -n 1"

$_CANDUMP_QUERY &> /tmp/$_FILE_PATH_CANDUMP_RESULT

set +x
exit 0
