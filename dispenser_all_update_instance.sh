#!/bin/bash
################################################################################
#
# Filename: dispenser_all_update_instance.sh
#   Author: Benjamin GUY-SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-07-26
#
################################################################################
#set -x

source ./libsh/print.sh

if [[ $# -ne 1 ]]; then
  print_formatted error "Usage, $0 <version-number-in-y.zz>"
  exit 3
fi

NAME_CUR_DISTRIBUTEUR='DISTRIBUTEUR'
NAME_NEW_DISTRIBUTEUR='NEW_DISTRIBUTEUR'
NAME_OLD_DISTRIBUTEUR='OLD_DISTRIBUTEUR'

PATH_TO_SOURCES='/home/phyvm/b2c/my-own/distributor/source'
PATH_TO_DISTRIB_INST='/home/distrib_inst.txt'

_RELATIVE_PATH_TO_DESKTOP_VERSION='distrib-Desktop_Qt_5_3_GCC_64bit-Debug'
_RELATIVE_PATH_TO_DEBUG_VERSION='distrib-phyBOARD_Wega-Debug'
_RELATIVE_PATH_TO_RELEASE_VERSION='distrib-phyBOARD_Wega-Release'

VERSION_NUMBER=$1

declare -a a_assets=('default.qss' \
                     'default_maint.qss')
a_assets_length=${#a_assets[@]}
a_assets_length_minus_1=$((a_assets_length - 1))

print_formatted info "Nom du binaire pour le  distributeur: ${NAME_CUR_DISTRIBUTEUR}"
print_formatted info "Version du binaire pour distributeur: ${VERSION_NUMBER}"

# On s'assure que la compilation a ete realise lors d'une release et non d'une feature

# On sauvegarde la version précédente du distributeur
print_formatted ok "Sauvegarde de la version précédente du distributeur sur le serveur OVH."
ssh -T ovh-server << EOSSH
mv /home/${NAME_NEW_DISTRIBUTEUR} /home/${NAME_OLD_DISTRIBUTEUR}
EOSSH

if [[ ! -f ${PATH_TO_SOURCES}/$_RELATIVE_PATH_TO_RELEASE_VERSION/${NAME_CUR_DISTRIBUTEUR} ]]; then
  print_formatted error "Erreur: $0, la nouvelle version du distributeur n'a pas été trouvé."
  print_formatted fail  "Arrêt du script!"
  exit 1
fi

# On copie la nouvelle version du distributeur sur le serveur ovh.
print_formatted info "Envoie de la nouvelle version du distributeur sur le serveur OVH."
#scp ${PATH_TO_SOURCES}/$_RELATIVE_PATH_TO_DEBUG_VERSION/${NAME_CUR_DISTRIBUTEUR} \
scp ${PATH_TO_SOURCES}/$_RELATIVE_PATH_TO_RELEASE_VERSION/${NAME_CUR_DISTRIBUTEUR} \
ovh-server:/home/${NAME_NEW_DISTRIBUTEUR}

# On créer une copie de NEW_DISTRIBUTEUR avec le nom DISTRIBUTEUR et le numéro de version.
print_formatted info "Copie de ${NAME_NEW_DISTRIBUTEUR} en ${NAME_CUR_DISTRIBUTEUR}_V${VERSION_NUMBER}."
ssh -T ovh-server << EOSSH
cp -a /home/${NAME_NEW_DISTRIBUTEUR} /home/${NAME_CUR_DISTRIBUTEUR}_V${VERSION_NUMBER}
EOSSH

# On copie les (nouveaux) assets pour le distributeur sur le serveur ovh.
print_formatted info "Envoie des assets pour la nouvelle version du distributeur sur le serveur OVH."
for i in $(seq 0 $a_assets_length_minus_1)
do
  if [[ ! -f ${PATH_TO_SOURCES}/$_RELATIVE_PATH_TO_DESKTOP_VERSION/${a_assets[i]} ]]; then
    print_formatted error "Erreur: $0, l'asset suivant pour le distributeur n'a pas été trouvé."
    print_formatted error "${a_assets[i]}"
    print_formatted fail  "Arrêt du script!"
    exit 2
  fi

  scp ${PATH_TO_SOURCES}/$_RELATIVE_PATH_TO_DESKTOP_VERSION/${a_assets[i]} \
  ovh-server:/home/
done

# On remplace le numéro de version sur le serveur ovh.
ssh -T ovh-server << EOSSH
echo "${NAME_CUR_DISTRIBUTEUR}_V${VERSION_NUMBER}" > ${PATH_TO_DISTRIB_INST}
EOSSH

#set +x
exit 0
