#!/bin/bash
################################################################################
#
# Filename: autostart_google_documentation_links.sh
#   Author: Benjamin GUY-SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-09-19
#
################################################################################
#set -x

################################################################################
#                                Environnement
################################################################################
source ./libsh/print.sh

_GOOGLE_CHROME=$(which google-chrome)

declare -a a_localhost_ports=('8000' \
                              '8001' \
                              '8002' \
                              '8003' \
                              '8004' \
                              '8005' )
a_localhost_ports_length=${#a_localhost_ports[@]}
a_localhost_ports_length_minus_1=$((a_localhost_ports_length - 1))

################################################################################
#                                  Définition
################################################################################
open_google_chrome_buffer()
{
    url=$1

    $_GOOGLE_CHROME --password-store=basic $url 1> /dev/null 2>&1 &
    unset url
}

################################################################################
#                                  Exécution
################################################################################
if [[ ! -f $_GOOGLE_CHROME ]]; then
  echo "$0, error: Google Chrome webbrowser not found!"
  exit 1
fi

print_formatted info "Démarrage de google-chrome avec les pages utiles."

for i in $(seq 0 $a_localhost_ports_length_minus_1)
do
  open_google_chrome_buffer http://localhost:${a_localhost_ports[i]}
  sleep 0.5
done

# Affichage Doxygen pour le distributeur.
open_google_chrome_buffer file:///home/phyvm/b2c/my-own/doxygen/dispenser/html/index.html

# Affichage dotfiles-2.0 documentation.
open_google_chrome_buffer http://localhost:8006

# Affichage workspace documentation
open_google_chrome_buffer http://localhost:8007

# Affichage programmation-2.0 documentation
open_google_chrome_buffer http://localhost:8008

set +x
exit 0
