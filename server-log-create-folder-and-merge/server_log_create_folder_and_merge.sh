#!/bin/bash
################################################################################
#
# Filename: server_log_create_folder_and_merge.sh
#   Author: Benjamin GUY-SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-10-23
#
################################################################################
#set -x

#source ./libsh/print.sh

################################################################################
#                                Environnement
################################################################################
_DEBUG="false"

# -a: Tableau simple (sh)
declare -a a_station_ids=(02 06 13 18 22 23 24 25 26 27 28 30 31 32 33 37 40)

# -A: Tableau multi dimension (bash)
declare -A a_station_names
a_station_names[02]='ELBEUF'
a_station_names[06]='DEVELOPMENT'
a_station_names[13]='MARINES'
a_station_names[18]='SAINT-WITZ'
a_station_names[22]='LES ECUELLES'
a_station_names[23]='MOUILLERON'
a_station_names[24]='BERGON'
a_station_names[25]=''
a_station_names[26]=''
a_station_names[27]=''
a_station_names[28]='PACY'
a_station_names[30]='BOUGUIN'
a_station_names[31]='AUDINCOURT'
a_station_names[32]='ERNEE'
a_station_names[33]='FLEXICOURT'
a_station_names[37]='FLEXICOURT B'
a_station_names[40]='CHEMILLE ANJOU'

# -A: Tableau multi dimension (bash)
declare -A a_station_headers
a_station_headers[02]='HEURTAUX SAS'
a_station_headers[06]='STATION DE TEST'
a_station_headers[13]='MOUSS AUTO'
a_station_headers[18]='GFC LAVAGE AUTOS SAS'
a_station_headers[22]='MOUSS AUTO 27'
a_station_headers[23]='GARAGE NICOLAS'
a_station_headers[24]='CENTRE RIVIERA SERVICE'
a_station_headers[25]='LE NAUTILUS'
a_station_headers[26]='DUNOIS DISTRIBUTION'
a_station_headers[27]='SASU ROMOND LAVAGE'
a_station_headers[28]='SARL JEDAV'
a_station_headers[30]='SAS IDM LAVAGE AUTO ROUTE'
a_station_headers[31]='PMA SERVICES'
a_station_headers[32]='LAVAGE DE LA MISSION'
a_station_headers[33]='TERRIER LAVAGE'
a_station_headers[37]='TERRIER LAVAGE'
a_station_headers[40]='GARAGE THOMAS'


################################################################################
#                                  Définiton 
################################################################################
translate_spaces_in_dash()
{
    # translate_spaces_in_dash est exécuté séparement de add_id_n_lower_case
    # pour pouvoir utilisé '$*' et récupérer l'intégralité des arguments
    # passés en paramêtres.
    translate_string=$*

    translate_string=$(echo $translate_string | tr ' ' '-')

    echo  $translate_string
    unset translate_string
}

add_id_n_lower_case()
{
    station_id=$1
    station_name=$2
    station_header=$3

    # Verifier utilisation $station_header a la place de station_name
    # l' inverse serait plus logique
    if [ "$station_header" = '' ]; then
	tmp_string=${station_id}-${station_name}
    else
	tmp_string=${station_id}-${station_name}-${station_header}
    fi
    tmp_string=$(echo $tmp_string | tr '[:upper:]' '[:lower:]')

    echo $tmp_string
    unset station_id
    unset station_header
    unset tmp_string
}

create_dir_if_does_not_exists()
{
    directory_name=$1

    if [ ! -d $directory_name ]; then
	mkdir $directory_name
    #else
	#echo "$directory_name already exists."
    fi
    
    unset directory_name
}

get_date_year_name_format()
{
    # L'année au format YYYY
    date +%Y
}

get_date_year_number_format()
{
    # L'année au format yy
    date +%y
}

get_date_month_name_format()
{
    # Le mois au format <month-name>
    date +%B
}

get_date_month_number_format()
{
    # Le mois au format mm
    date +%m
}

get_date_day_number_format()
{
    # Le jour au format dd
    date +%d
}

get_station_id()
{
    full_path_directory=$1

    # Récupérer l'id de la station pour préfix le fichier de logs à merger.
    ls -1 $full_path_directory | head -n 1 | cut -d '_' -f 1

    unset full_path_directory
}

print_debug_variable()
{
    if [ "$_DEBUG" = "true" ]; then
	echo -e 'Station name: \t\t'     ${a_station_names[$station_id]}
	echo -e 'Dispenser header: \t'   ${a_station_headers[$station_id]}
	echo -e 'Translate name: \t'     $translate_name
	echo -e 'Translate header: \t'   $translate_header
	echo -e 'Name for directory: \t' $name_for_directory
	echo ''
    fi
}

substract_day_to_current_date()
{
    # date -d "$dataset_date - $date_diff days" +%Y-%m-%d
    # Where:
    # 1. -d --------------------------------- options, in this case 
    #                                         followed need to be date 
    #                                         in string format (look up on $ man date)
    # 2. "$dataset_date - $date_diff days" -- date arithmetic, more 
    #                                         have a look at article by [PETER LEUNG][1]
    # 3. +%Y-%m-%d -------------------------- your desired format, year-month-day

    current_date=$1
    number_of_days=$2

    # TODO: à utiliser a la place de la soustraction de current_date et number_of_days
    # date -d "$current_date - $number_of_days day" +%d
    date_minus_1=$((current_date - number_of_days))
    echo $date_minus_1

    unset number_of_days
    unset date_minus_1
}

find_all_logs_and_merge_to_one()
{
    full_path_directory=$1
    name_file=$2
    logs_file_to_merge=$3
    date_minus_1=$4

    # C'est log_file_to_merge qui n' est pas bon

    # - Ajouter le contenu de chacun des fichiers trouvés dans un seul et même fichier. 
    for log_file_to_merge in ${logs_file_to_merge[@]}
    do

	if [ -f $full_path_directory/$log_file_to_merge ]; then
	    cat $full_path_directory/$log_file_to_merge \
		>>  $full_path_directory/$(get_station_id $full_path_directory)_${name_file}_${date_minus_1}.txt
	    rm  $full_path_directory/$log_file_to_merge
	else
	    echo $full_path_directory/$log_file_to_merge "doesn't exists."
	fi

	unset log_file_to_merge
    done

    # /!\ Ne pas unset la variable full_path_directory à cet endroit,
    # sinon lors de l'appel find_all_logs_and_merge_to_one $full_path_directory paramLog "${paramLog_files_to_merge[@]}" ${_DATE_YMD_NUMBER_FORMAT_MINUS_1}
    # $full_path_directory sera vide et tous les parametres seront decales de 1 index.

    #unset full_path_directory
    unset name_file
    unset date_minus_1
}


################################################################################
#                                  Exécution 
################################################################################
# 1. Exemple de valeurs retournées ou assignées par le script (mise en commentaires).
# 2. Fonction qui calcule la valeur par rapport à la date de la machine sur laquelle il est éxécuté.

#_DATE_YEAR_NAME_FORMAT=2018
_DATE_YEAR_NAME_FORMAT=$(get_date_year_name_format)

#_DATE_YEAR_NUMBER_FORMAT=18
_DATE_YEAR_NUMBER_FORMAT=$(get_date_year_number_format)

#_DATE_MONTH_NAME_FORMAT=December
_DATE_MONTH_NAME_FORMAT=$(get_date_month_name_format)

#_DATE_MONTH_NUMBER_FORMAT=12
_DATE_MONTH_NUMBER_FORMAT=$(get_date_month_number_format)

#_DATE_DAY_NUMBER_FORMAT=02
_DATE_DAY_NUMBER_FORMAT=$(get_date_day_number_format)

#_DATE_YMD_NUMBER_FORMAT=181202
_DATE_YMD_NUMBER_FORMAT=${_DATE_YEAR_NUMBER_FORMAT}${_DATE_MONTH_NUMBER_FORMAT}${_DATE_DAY_NUMBER_FORMAT}

if [ "$_DEBUG" = "true" ]; then
    echo -e 'Date year name format: \t\t'  $_DATE_YEAR_NAME_FORMAT
    echo -e 'Date month name format: \t'   $_DATE_MONTH_NAME_FORMAT
    echo -e 'Date year number format: \t'  $_DATE_YEAR_NUMBER_FORMAT
    echo -e 'Date month number format: \t' $_DATE_MONTH_NUMBER_FORMAT
fi

# Pour chaque station récupérer:
# - l'identifiant
# - le nom alternatif
# - l'entête ticket
for station_id in ${a_station_ids[@]}
do

    # Créer le nom du dossier parent:
    # - Remplacer les espaces par des tirets
    translate_name=$(translate_spaces_in_dash   ${a_station_names[$station_id]})
    translate_header=$(translate_spaces_in_dash ${a_station_headers[$station_id]})

    # - Concatener l'identifiant, l'entête, la désignation
    # - Convertir les majuscules en minuscules
    name_for_directory=$(add_id_n_lower_case \
			     $station_id     \
			     $translate_name \
			     $translate_header)

    # Créer les dossier et sous dossiers:
    # - Utiliser le nom de dossier parent
    # - Créer un sous dossier année (format 4 caractères)
    # - Créer un sous dossier mois  (format X carcactères)
    create_dir_if_does_not_exists $name_for_directory
    create_dir_if_does_not_exists $name_for_directory/$_DATE_YEAR_NAME_FORMAT
    create_dir_if_does_not_exists $name_for_directory/$_DATE_YEAR_NAME_FORMAT/$_DATE_MONTH_NAME_FORMAT

    print_debug_variable

    # Sauvegarder l'intégralité de l'arborescence pour la parcours dans la boucle pour merger les fichiers de logs
    declare -a directories_to_browse=("${directories_to_browse[@]}" "$name_for_directory")
done

# Récupérer la date courante et retirer un jour pour obtenir la veille
# TODO:
# Modifier script pour prendre en charge les jours avec un 0 devant le second chiffre.
#_DATE_DAY_NUMBER_FORMAT_MINUS_1=03
_DATE_DAY_NUMBER_FORMAT_MINUS_1=$(substract_day_to_current_date $_DATE_DAY_NUMBER_FORMAT 1)
_DATE_YMD_NUMBER_FORMAT_MINUS_1=${_DATE_YEAR_NUMBER_FORMAT}${_DATE_MONTH_NUMBER_FORMAT}${_DATE_DAY_NUMBER_FORMAT_MINUS_1}

# - Merger les fichiers de la veille en un seul
for directory_to_browse in ${directories_to_browse[@]}
do
    # - Sauvegarder l'intégralité du path pour le dossier à parcourir
    full_path_directory=$directory_to_browse/$_DATE_YEAR_NAME_FORMAT/$_DATE_MONTH_NAME_FORMAT
    
    # - Référencer tous les fichiers de logs contenu dans un dossier
    appliLog_files_to_merge=$(ls -1 $full_path_directory | grep _appliLog_${_DATE_YMD_NUMBER_FORMAT_MINUS_1})
    paramLog_files_to_merge=$(ls -1 $full_path_directory | grep _paramLog_${_DATE_YMD_NUMBER_FORMAT_MINUS_1})

    [ "$_DEBUG" = "true" ] && [ ! -z "$appliLog_files_to_merge" ] && echo $appliLog_files_to_merge
    [ "$_DEBUG" = "true" ] && [ ! -z "$paramLog_files_to_merge" ] && echo $paramLog_files_to_merge

    # - Ajouter le contenu de chacun des fichiers trouvés dans un seul et même fichier. 
    find_all_logs_and_merge_to_one $full_path_directory appliLog "${appliLog_files_to_merge[@]}" ${_DATE_YMD_NUMBER_FORMAT_MINUS_1}
    find_all_logs_and_merge_to_one $full_path_directory paramLog "${paramLog_files_to_merge[@]}" ${_DATE_YMD_NUMBER_FORMAT_MINUS_1}

    unset appliLog_files_to_merge
    unset paramLog_files_to_merge
done

set +x
exit 0
