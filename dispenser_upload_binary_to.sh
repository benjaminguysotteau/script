#!/bin/bash
################################################################################
#
# Filename: dispenser_upload_binary_to.sh
#   Author: Benjamin GUY SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2018-06-29
#
################################################################################
#set -x

# SOURCE
# ------
# https://stackoverflow.com/questions/626533/how-can-i-ssh-directly-to-a-particular-directory

# Pour que ./libsh/ fonctionne il faut faire
# un cd dans le fichier appelant autostart.sh
source ./libsh/print.sh

BINARY='DISTRIBUTEUR'
VERSION='1.60.0'
PATH_TO_PROJECT='b2c/my-own/distributor/source'
#_PATH_TO_VERSION='distrib-phyBOARD_Wega-Release'
_PATH_TO_VERSION='distrib-phyBOARD_Wega-Debug'

print_formatted info "Nom du binaire:     ${BINARY}"
print_formatted info "Version du binaire: ${VERSION}"
print_formatted info "Chemin du projet:   ${PATH_TO_PROJECT}"
print_formatted info "Chemin de la version: $_PATH_TO_VERSION"

# On se connecte au distributeur,
# on arrête l'éxécution du programme DISTRIBUTEUR,
# on envoie le nouveau binaire distributeur et les assets,
ssh -T root@dev-dispenser << EOSSH
cd /home
./kill_binary.sh 2> /dev/null
#cp -a ${BINARY} /media/sdcard/bckp_${BINARY}_${VERSION}_$(date +%Y%m%d)_$(date +%H%M%S)
EOSSH
print_formatted ok "Arrêt du binaire actuel en cours d'éxécution."
#print_formatted ok "Copie de l'ancienne version du binaire sur la carte sd."

# On demande un redemarrage du distributeur.
if [[ $# -gt 2 ]]; then
  echo "Usage: $0 <all> <reboot>"
  echo "Usage: $0 <reboot>"
  exit 1
fi

# On envoie aussi les nouveaux assets.
if [[ "$1" == "all" ]]; then
  scp -p ${HOME}/${PATH_TO_PROJECT}/distrib-Desktop_Qt_5_3_GCC_64bit-Debug/*.qss \
  root@dev-dispenser:/home/
fi

print_formatted info "Envoie de ${BINARY} vers le distributeur de développement"
scp -p ${HOME}/${PATH_TO_PROJECT}/$_PATH_TO_VERSION/${BINARY} \
root@dev-dispenser:/home/

if [[ "$1" == "reboot" ]] || [[ "$2" == "reboot" ]]; then
  ssh root@dev-dispenser "reboot"
else
  ssh -t root@dev-dispenser "cd /home/; bash;"
fi

#set +x
exit 0
