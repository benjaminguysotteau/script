#!/usr/bin/env bash
################################################################################
#
# Filename: dispenser_all_get_logs.sh
#   Author: Benjamin GUY-SOTTEAU
#     Mail: benjamin.guysotteau@heurtaux.fr
#  Created: 2019-02-05
#
################################################################################
#set -x

# Objectif:
# ---------
# Pour chaque distributeur, copier les logs en carte SD (si présent)sur le serveur
# centralisé, puis les supprimer pour garder un stockage propre
# et celon le système de fichier de la carte SD, supprimer les droits d'éxécution
# pour les logs copier sur le serveur centralisé.

################################################################################
#                                Environnement
################################################################################
#source ./libsh/print.sh

# CONSTANTS
                          # Exemple 3 mars 2019
_YEAR_NUMBER=$(date +%y)  # 19
_YEAR_LETTER=$(date +%Y)  # 2019
_MONTH_NUMBER=$(date +%m) # 03
_MONTH_LETTER=$(date +%B) # March

# Dossier ou les fichiers en carte SD sont copiés sur le serveur.
_PATH_DIR_DOWNLOAD="/tmp/download-from-dispenser"

# Déclaration tableau associatif avec pour
# clé:    L'id de la station.
# valeur: Le chemin sur le serveur centralisé (ville + nom).
declare -A stations=(
    [02]="elbeuf-heurtaux-sas"
    [06]="development-station-de-test"
    [13]="marines-mouss-auto"
    [18]="saint-witz-gfc-lavage-autos-sas"
    [22]="les-ecuelles-mouss-auto-27"
    [23]="mouilleron-garage-nicolas"
    [24]="bergon-centre-riviera-service"
    [25]="le-nautilus"
    [26]="dunois-distribution"
    [27]="sasu-romond-lavage"
    [28]="pacy-sarl-jedav"
    [30]="bouguin-sas-idm-lavage-auto-route"
    [31]="audincourt-pma-services"
    [32]="ernee-lavage-de-la-mission"
    [33]="flexicourt-terrier-lavage"
    [37]="flexicourt-b-terrier-lavage"
    [40]="chemille-anjou-garage-thomas"
)

# PARAMETERS

################################################################################
#                                  Définition
################################################################################
# Copier les logs d'une carte SD d'un distributeur sur le serveur centralisé.
# On affiche la commande avant de l'éxécuter pour rendre le script le plus verbeux possible.
# $1:  L'id de la station qui est l'index du tableau associatif.
# $2:  Le chemin composer de la ville et d'un nom, correspond à la valeur du tableau associatif.
# cmd: La commande à afficher et à éxécuter.
copy_logs_from_ssh()
{
    id=$1
    path=$2
    _CMD="scp -P 220${id} -o StrictHostKeyChecking=no -p root@127.0.0.1:/media/sdcard/${id}_*Log_${_YEAR_NUMBER}${_MONTH_NUMBER}*.txt $_PATH_DIR_DOWNLOAD"

    echo "Info: $_CMD"
    $_CMD 2> /dev/null

    # On unset id dans la boucle for, sinon il n'éxiste plus pour une commande suivante.
    #unset id
    unset path
    unset _CMD
}

# Effacer les logs d'une carte SD d'un distributeur depuis le serveur centralisé.
# On affiche la commande avant de l'éxécuter pour rendre le script le plus verbeux possible.
# On laisse 4 espaces avant root pour l'affiche par rapport à la commande précendente.
# $1:    L'id de la station, correspond à l'index du tableau associatif.
# clean: La commande à afficher et à éxécuter.
clean_logs_from_ssh()
{
    id=$1
    _CLEAN="ssh -p 220${id} -o StrictHostKeyChecking=no    root@127.0.0.1 \"rm -f /media/sdcard/${id}_*Log_${_YEAR_NUMBER}${_MONTH_NUMBER}*.txt\""

    echo "Info: $_CLEAN"
    # /!\ Utilisation d'eval par la contante _CLEAN car 2x des doubles guillements,
    #     ce qui est ma interprêté sans le eval!
    eval $_CLEAN 2> /dev/null

    # On unset id dans la boucle for, sinon il n'éxiste plus pour une commande suivante.
    #unset id
    unset _CLEAN
}

# Changer les droits (retirer l'éxécution) pour les logs copier sur le serveur centralisé.
# On affiche la commande avant de l'éxécuter pour rendre le script le plus verbeux possible.
# $1:     L'id de la station, correspond à l'index du tableau associatif.
# rights: La commande à afficher et à éxécuter.
change_rights_logs()
{
    id=$1
    _RIGHTS="chmod -x ${id}-${stations[$id]}/$_YEAR_LETTER/$_MONTH_LETTER/${id}_*Log_${_YEAR_NUMBER}${_MONTH_NUMBER}*.txt"

    echo "Info: $_RIGHTS"
    $_RIGHTS 2> /dev/null

    # On unset id dans la boucle for, sinon il n'éxiste plus pour une commande suivante.
    #unset id
    unset _RIGHTS
}

# Si le dossier ou copier les fichiers en carte SD sur le serveur n'éxiste pas, on le créer.
create_download_dir()
{
    path=$1
    _DIR="mkdir -p $path"

    if [ ! -d "$path" ]; then
        echo "Info: $_DIR"
        $_DIR
    fi

    unset path
    unset _DIR
}

################################################################################
#                                  Exécution
################################################################################
# Lors de la copie des logs, on cible le dossier dans /tmp et non le dossier dédié.
# Car si un envoi a échoué précédement par distributeur mais que le suivant est passé, l'upload sur le serveur
# direcement dans le dossier dédié va écraser le fichier le plus récent avec un plus ancien présent en carte SD.
#
# Pour chaque distributeur, on copie les logs de la carte SD sur le serveur centralisé.
# On efface les logs de la carte SD du distributeur pour la garder propre,
# On change les droits des logs copiés sur le serveur centralisé en retirant les droits d'éxécution.

create_download_dir $_PATH_DIR_DOWNLOAD

for id in "${!stations[@]}"; do

    copy_logs_from_ssh  $id ${stations[$id]}
    # TODO: clean n change rights on file only if new file are copied.
    clean_logs_from_ssh $id
    change_rights_logs  $id ${stations[$id]}
    echo

    unset id

done

#set +x
exit 0
